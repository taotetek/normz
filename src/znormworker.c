/*  =========================================================================
    znormworker - Normalization worker

    Copyright (c) the Contributors as noted in the AUTHORS file.       
    This file is part of CZMQ, the high-level C binding for 0MQ:       
    http://czmq.zeromq.org.                                            
                                                                       
    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.           
    =========================================================================
*/

/*
@header
    znormworker - Normalization worker
@discuss
@end
*/

#include "normz_classes.h"

//  Structure of our actor

struct _znormworker_t {
    zsock_t *pipe;              //  Actor command pipe
    zpoller_t *poller;          //  Socket poller
    znormalizer_t *normalizer;  //  znormalizer instance
    zcert_t *client_cert;       //  client curve cert
    zcert_t *server_cert;       //  server curve cert
    zsock_t *frontend;          //  front socket
    zsock_t *backend;           //  back socket
    bool terminated;            //  Did caller ask us to quit?
    bool verbose;               //  Verbose logging enabled?
    //  TODO: Declare properties
};


//  --------------------------------------------------------------------------
//  Create a new znormworker instance

static znormworker_t *
znormworker_new (zsock_t *pipe, void *args)
{
    znormworker_t *self = (znormworker_t *) zmalloc (sizeof (znormworker_t));
    assert (self);

    self->pipe = pipe;
    self->terminated = false;
    self->poller = zpoller_new (self->pipe, NULL);
    self->normalizer  = znormalizer_new ();
    assert  (self->normalizer);

    return self;
}


//  --------------------------------------------------------------------------
//  Destroy the znormworker instance

static void
znormworker_destroy (znormworker_t **self_p)
{
    assert (self_p);
    if (*self_p) {
        znormworker_t *self = *self_p;
        zcert_destroy (&self->client_cert);
        zcert_destroy (&self->server_cert);
        zpoller_destroy (&self->poller);
        zsock_destroy (&self->frontend);
        zsock_destroy (&self->backend);
        znormalizer_destroy (&self->normalizer);

        free (self);
        *self_p = NULL;
    }
}


//  Start this actor. Return a value greater or equal to zero if initialization
//  was successful. Otherwise -1.

static int
znormworker_start (znormworker_t *self)
{
    assert (self);
    assert (self->poller);
    assert (self->frontend);
    assert (self->backend);

    zpoller_add (self->poller, self->frontend);
    zpoller_add (self->poller, self->backend);

    //  TODO: Add startup actions

    return 0;
}


//  Stop this actor. Return a value greater or equal to zero if stopping 
//  was successful. Otherwise -1.

static int
znormworker_stop (znormworker_t *self)
{
    assert (self);

    //  TODO: Add shutdown actions

    return 0;
}


static zsock_t *
s_create_socket (znormworker_t *self, char *type_name, char  *endpoints)
{
    char *type_names [] = {
        "PAIR", "PUB", "SUB", "REQ", "REP",
        "DEALER", "ROUTER", "PULL", "PUSH",
        "XPUB", "XSUB", type_name
    };

    int i;
    for (i = 0; strneq (type_name, type_names [i]); i++) ;
    if (i > ZMQ_XSUB) {
        return NULL;
    }
    zsock_t *sock = zsock_new (i);
    if (sock) {
        if (zsock_attach (sock, endpoints, true)) {
            zsock_destroy (&sock);
        }
    }
    return sock;
}

static void
s_self_switch (znormworker_t *self, zsock_t *input, zsock_t *output)
{
    // normalizer logic goes here
}

//  Here we handle incoming message from the node

static void
znormworker_recv_api (znormworker_t *self)
{
    //  Get the whole message of the pipe in one go
    zmsg_t *request = zmsg_recv (self->pipe);
    if (!request)
       return;        //  Interrupted

    char *command = zmsg_popstr (request);
    if (streq (command, "START"))
        znormworker_start (self);
    else
    if (streq (command, "STOP"))
        znormworker_stop (self);
    else
    if (streq (command, "VERBOSE"))
        self->verbose = true;
    else
    if (streq (command, "RULEBASE")) {
        char *path = zmsg_popstr (request);
        int rc = znormalizer_load_rulebase (self->normalizer, path);
        zstr_free (&path);
        zsock_signal (self->pipe, rc);
    }
    else
    if (streq (command, "FRONTEND")) {
        int rc = 0;
        char *type_name = zmsg_popstr (request);
        char *endpoints = zmsg_popstr (request);
        self->frontend = s_create_socket (self, type_name,  endpoints);
        if (!self->frontend)
           rc = -1; 
        
        zstr_free (&type_name);
        zstr_free (&endpoints);
        zsock_signal (self->pipe, rc);
    }
    else
    if (streq (command, "BACKEND")) {
        int rc = 0;
        char *type_name = zmsg_popstr (request);
        char *endpoints = zmsg_popstr (request);
        self->backend = s_create_socket (self, type_name,  endpoints);
        if (!self->backend)
           rc = -1; 
        
        zstr_free (&type_name);
        zstr_free (&endpoints);
        zsock_signal (self->pipe, rc);
    }
    else
    if (streq (command, "BACKEND")) {
        zsock_signal (self->pipe, 0);
    }
    else
    if (streq (command, "$TERM"))
        //  The $TERM command is send by zactor_destroy() method
        self->terminated = true;
    else {
        zsys_error ("invalid command '%s'", command);
        assert (false);
    }
    zstr_free (&command);
    zmsg_destroy (&request);
}


//  --------------------------------------------------------------------------
//  This is the actor which runs in its own thread.

void
znormworker_actor (zsock_t *pipe, void *args)
{
    znormworker_t * self = znormworker_new (pipe, args);
    if (!self)
        return;          //  Interrupted

    //  Signal actor successfully initiated
    zsock_signal (self->pipe, 0);

    while (!self->terminated) {
        zsock_t *which = (zsock_t *) zpoller_wait (self->poller, 0);
        if (zpoller_terminated (self->poller))
            break;
        else
        if (which == self->pipe)
            znormworker_recv_api (self);
        else
        if (which == self->frontend)
            s_self_switch (self, self->frontend, self->backend);
        else
        if (which == self->backend)
            s_self_switch  (self, self->backend, self->frontend);
    }
    znormworker_destroy (&self);
}

//  --------------------------------------------------------------------------
//  Self test of this actor.

void
znormworker_test (bool verbose)
{
    printf (" * znormworker: ");
    //  @selftest
    //  Simple create/destroy test
    // Note: If your selftest reads SCMed fixture data, please keep it in
    // src/selftest-ro; if your test creates filesystem objects, please
    // do so under src/selftest-rw. They are defined below along with a
    // usecase (asert) to make compilers happy.
    const char *SELFTEST_DIR_RO = "src/selftest-ro";
    const char *SELFTEST_DIR_RW = "src/selftest-rw";
    assert (SELFTEST_DIR_RO);
    assert (SELFTEST_DIR_RW);
    // Uncomment these to use C++ strings in C++ selftest code:
    //std::string str_SELFTEST_DIR_RO = std::string(SELFTEST_DIR_RO);
    //std::string str_SELFTEST_DIR_RW = std::string(SELFTEST_DIR_RW);
    //assert ( (str_SELFTEST_DIR_RO != "") );
    //assert ( (str_SELFTEST_DIR_RW != "") );
    // NOTE that for "char*" context you need (str_SELFTEST_DIR_RO + "/myfilename").c_str()

    zactor_t *normalizer = zactor_new (znormworker_actor, NULL);
    assert (normalizer);

    zstr_sendx (normalizer, "RULEBASE", "src/selftest-ro/sample.rulebase", NULL);
    zsock_wait (normalizer);

    zstr_sendx (normalizer, "FRONTEND", "PULL", "inproc://frontend", NULL);
    zsock_wait (normalizer);

    zstr_sendx (normalizer, "BACKEND", "PUSH", "inproc://backend", NULL);
    zsock_wait (normalizer);

    zactor_destroy (&normalizer);
    //  @end

    printf ("OK\n");
}
