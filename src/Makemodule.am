################################################################################
#  THIS FILE IS 100% GENERATED BY ZPROJECT; DO NOT EDIT EXCEPT EXPERIMENTALLY  #
#  Read the zproject/README.md for information about making permanent changes. #
################################################################################
program_libs = src/libnormz.la ${project_libs}

# Programs need to link the c++ runtime if everything was compiled statically.
if !ENABLE_SHARED
program_libs += -lstdc++ -lm
endif

lib_LTLIBRARIES += src/libnormz.la
pkgconfig_DATA = src/libnormz.pc

include_HEADERS = \
    include/normz.h \
    include/normz_library.h

if ENABLE_DRAFTS
include_HEADERS += \
    include/znormalizer.h \
    include/znormworker.h

endif
src_libnormz_la_SOURCES = \
    src/platform.h

if ENABLE_DRAFTS
src_libnormz_la_SOURCES += \
    src/znormalizer.c \
    src/znormworker.c

endif

if ENABLE_DRAFTS
src_libnormz_la_SOURCES += \
    src/normz_private_selftest.c
endif

src_libnormz_la_CPPFLAGS = ${AM_CPPFLAGS}

src_libnormz_la_LDFLAGS = \
    -version-info @LTVER@ \
    $(LIBTOOL_EXTRA_LDFLAGS)

if ON_MINGW
src_libnormz_la_LDFLAGS += \
    -no-undefined \
    -avoid-version
endif

if ON_CYGWIN
src_libnormz_la_LDFLAGS += \
    -no-undefined \
    -avoid-version
endif

src_libnormz_la_LIBADD = ${project_libs}

if ENABLE_NORMZ
bin_PROGRAMS += src/normz
src_normz_CPPFLAGS = ${AM_CPPFLAGS}
src_normz_LDADD = ${program_libs}
src_normz_SOURCES = src/normz.c
endif #ENABLE_NORMZ

if ENABLE_NORMZ_SELFTEST
check_PROGRAMS += src/normz_selftest
noinst_PROGRAMS += src/normz_selftest
src_normz_selftest_CPPFLAGS = ${AM_CPPFLAGS}
src_normz_selftest_LDADD = ${program_libs}
src_normz_selftest_SOURCES = src/normz_selftest.c
endif #ENABLE_NORMZ_SELFTEST

# define custom target for all products of /src
src: \
		src/normz \
		src/normz_selftest \
		src/libnormz.la


# Directories with test fixtures optionally provided by the project,
# and with volatile RW data possibly created by a selftest program.
# It is up to the project authors to populate the RO directory with
# filenames called from the selftest methods, if any. They will be
# EXTRA_DISTed by the recipes generated with with zproject, however,
# and copied into builddir (if different from srcdir) to simplify
# the "distcheck" and similar tests (so selftest can use same paths).
# Note that the RO directory must exist to fulfill EXTRA_DIST, so we
# add a stub file that can be committed to SCM by project developers.
# The RW directory will be automatically wiped by "make distclean".
SELFTEST_DIR_RO = src/selftest-ro
SELFTEST_DIR_RW = src/selftest-rw

$(abs_top_builddir)/$(SELFTEST_DIR_RW):
	mkdir -p "$@"

$(abs_top_builddir)/$(SELFTEST_DIR_RO): $(abs_top_srcdir)/$(SELFTEST_DIR_RO)
	@if test "$@" != "$<" ; then \
		echo "    COPYDIR  $(SELFTEST_DIR_RO)"; \
		rm -rf "$@"; \
		cp -r "$<" "$@" ; \
		fi

CLEANFILES += $(abs_top_builddir)/$(SELFTEST_DIR_RW)/*

# Note that this syntax dists the whole directory - including subdirs (if any)
EXTRA_DIST += $(SELFTEST_DIR_RO)

clean-local: clean-local-selftest-ro clean-local-selftest-rw
.PHONY: clean-local-selftest-ro
clean-local-selftest-ro:
	@if test "$(abs_top_builddir)" != "$(abs_top_srcdir)" ; then \
		if test -d "$(abs_top_builddir)/$(SELFTEST_DIR_RO)" ; then \
			chmod -R u+w "$(abs_top_builddir)/$(SELFTEST_DIR_RO)" ; \
			rm -rf "$(abs_top_builddir)/$(SELFTEST_DIR_RO)" ; \
		fi; \
	fi

# Unlike CLEANFILES setting above, this one whould also wipe created subdirs
.PHONY: clean-local-selftest-rw
clean-local-selftest-rw:
	@if test "$(abs_top_builddir)" != "$(abs_top_srcdir)" ; then \
		if test -d "$(abs_top_builddir)/$(SELFTEST_DIR_RW)" ; then \
			chmod -R u+w "$(abs_top_builddir)/$(SELFTEST_DIR_RW)" ; \
			rm -rf "$(abs_top_builddir)/$(SELFTEST_DIR_RW)" ; \
		fi; \
	fi

check-local: src/normz_selftest $(abs_top_builddir)/$(SELFTEST_DIR_RW) $(abs_top_builddir)/$(SELFTEST_DIR_RO)
	$(LIBTOOL) --mode=execute $(builddir)/src/normz_selftest

check-verbose: src/normz_selftest $(abs_top_builddir)/$(SELFTEST_DIR_RW) $(abs_top_builddir)/$(SELFTEST_DIR_RO)
	$(LIBTOOL) --mode=execute $(builddir)/src/normz_selftest -v

# Run the selftest binary under valgrind to check for memory leaks
memcheck: src/normz_selftest $(abs_top_builddir)/$(SELFTEST_DIR_RW) $(abs_top_builddir)/$(SELFTEST_DIR_RO)
	$(LIBTOOL) --mode=execute valgrind --tool=memcheck \
		--leak-check=full --show-reachable=yes --error-exitcode=1 \
		--suppressions=$(srcdir)/src/.valgrind.supp \
		$(builddir)/src/normz_selftest

# Run the selftest binary under valgrind to check for performance leaks
callcheck: src/normz_selftest $(abs_top_builddir)/$(SELFTEST_DIR_RW) $(abs_top_builddir)/$(SELFTEST_DIR_RO)
	$(LIBTOOL) --mode=execute valgrind --tool=callgrind \
		$(builddir)/src/normz_selftest

# Run the selftest binary under gdb for debugging
debug: src/normz_selftest $(abs_top_builddir)/$(SELFTEST_DIR_RW) $(abs_top_builddir)/$(SELFTEST_DIR_RO)
	$(LIBTOOL) --mode=execute gdb -q \
		$(builddir)/src/normz_selftest

# Run the selftest binary with verbose switch for tracing
animate: src/normz_selftest $(abs_top_builddir)/$(SELFTEST_DIR_RW) $(abs_top_builddir)/$(SELFTEST_DIR_RO)
	$(LIBTOOL) --mode=execute $(builddir)/src/normz_selftest -v

if WITH_GCOV
coverage: src/normz_selftest $(abs_top_builddir)/$(SELFTEST_DIR_RW) $(abs_top_builddir)/$(SELFTEST_DIR_RO)
	@echo "you had called configure --with-gcov"
	lcov --base-directory . --directory . --zerocounters -q
	$(MAKE) check
	lcov --base-directory . --directory . --capture -o coverage.info
	lcov --remove coverage.info "/usr*" -o coverage.info
	lcov --remove coverage.info "normz_selftest.c" -o coverage.info
	$(RM) -rf coverage/*
	genhtml -o coverage/ -t "normz test coverage" --num-spaces 4 coverage.info
else
coverage: src/normz_selftest
	@echo "call make clean && configure --with-gcov to enable code coverage"
	@exit 1
endif

################################################################################
#  THIS FILE IS 100% GENERATED BY ZPROJECT; DO NOT EDIT EXCEPT EXPERIMENTALLY  #
#  Read the zproject/README.md for information about making permanent changes. #
################################################################################
