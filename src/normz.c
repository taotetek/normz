/*  =========================================================================
    normalz - description

    Copyright (c) the Contributors as noted in the AUTHORS file.       
    This file is part of normalz.                                      
                                                                       
    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.           
    =========================================================================
*/

/*
@header
    normalz - 
@discuss
@end
*/

#include "normz_classes.h"

int main (int argc, char *argv [])
{
    zsys_set_logstream (stdout);

    bool verbose = false;
    bool arg_error = false;
    char *url = NULL;
    char *key = NULL;
    char *lib = NULL;
    char *endpoints = NULL;
    char *client_cert_path = NULL;
    char *server_cert_path = NULL;
    char *rulebase_path = NULL;
    char *topic = NULL;
    
    int argn;
    for (argn = 1; argn < argc; argn++) {
        if (streq (argv [argn], "--help")
        ||  streq (argv [argn], "-h")) {
            puts ("normalz [options] ...");
            puts ("  --verbose / -v         verbose test output");
            puts ("  --help / -h            this information");
            puts ("  --rulebase / -h        path to liblognorm rulebase");
            puts ("  --url / -u             url for service discovery system");
            puts ("  --key / -k             key for service lookup");
            puts ("  --lib / -l             path to go lib to use for discovery lookup");
            puts ("  --endpoints / -e       list of endpoints - do not use with --lib --url --key");
            puts ("  --clientcert / -c      path to client curve cert (ephemeral cert generated if not specified");
            puts ("  --servercert / -s      path to server server cert");
            puts ("  --topic / -t           topic to subscribe to");
            return 0;
        }

        if (streq (argv [argn], "--verbose")
        ||  streq (argv [argn], "-v")) {
            verbose = true;
        }
        else
        if (streq (argv [argn], "--rulebase")
        || streq (argv [argn], "-r")) {
            argn++;
            rulebase_path = argv [argn];
        }
        else
        if (streq (argv [argn], "--url")
        || streq (argv [argn], "-u")) {
            argn++;
            url = argv [argn];
        }
        else
        if (streq (argv [argn], "--key")
        || streq (argv [argn], "-e")) {
            argn++;
            key = argv [argn];
        }
        else
        if (streq (argv [argn], "--lib")
        || streq (argv [argn], "-l")) {
            argn++;
            lib = argv [argn];
        }
        else
        if (streq (argv [argn], "--clientcert")
        || streq (argv [argn], "-c")) {
            argn++;
            client_cert_path = argv [argn];
        }
        else
        if (streq (argv [argn], "--servercert")
        || streq (argv [argn], "-s")) {
            argn++;
            server_cert_path = argv [argn];
        }
        else
        if (streq (argv [argn], "--topic")
        || streq (argv [argn], "-t")) {
            argn++;
            topic = argv [argn];
        }
        else {
            zsys_error ("Uknown option: %s\n", argv [argn] );
            return 1;
        }
    }

    if (verbose) {
        zsys_debug ("normalz - verbose set");
        zsys_debug ("rulebase: %s", rulebase_path);
        zsys_debug ("url: %s", url);
        zsys_debug ("key: %s", key);
        zsys_debug ("lib: %s", lib);
        zsys_debug ("clientcert: %s", client_cert_path);
        zsys_debug ("servercert: %s", server_cert_path);
        zsys_debug ("topic: %s", topic);
    }

    if (!url) {
        zsys_error ("--url is required");
        arg_error = true; 
    }

    if (!key) {
        zsys_error ("--key is required");
        arg_error = true; 
    }

    if (!lib) {
        zsys_error ("--lib is required");
        arg_error = true;
    }

    if (!rulebase_path) {
        zsys_error ("--rulebase is required");
        arg_error = true;
    }

    if (!server_cert_path) {
        zsys_error ("--servercert is required");
        arg_error = true;
    }

    if (!topic) {
        topic = "";
    }
 
    if ((endpoints != NULL) && (lib != NULL)) {
        zsys_error ("cannot use both --endpoints and --lib");
        arg_error = true;
    }

    if (arg_error)
        return 1;

    // create the znormalizer
    znormalizer_t *normalizer = znormalizer_new ();
    if (!normalizer) {
        zsys_error ("could not create normalizer");
        return 1;
    }

    if (verbose)
        znormalizer_verbose (normalizer);

    int rc = znormalizer_load_rulebase (normalizer, rulebase_path);
    if (rc != 0) {
        zsys_error ("could not load rulebase '%s'", rulebase_path);
        return 1;
    }

    // create client socket
    zsock_t *client = zsock_new (ZMQ_SUB);
    if (!client) {
        zsys_error ("could not create socket");
        return 1;
    }

    // retrieve or create client cert 
    zcert_t *client_cert;
    if (client_cert_path) {
        client_cert = zcert_load (client_cert_path);
        if (!client_cert) {
            zsys_error ("could not load cert from '%s'", client_cert_path);
            return 1;
        }
    }
    else {
        client_cert = zcert_new ();
        if (!client_cert) {
            zsys_error ("--client_cert_path not specified and could not generate one");
            return 1;
        }
    }
    zcert_apply (client_cert, client);
    zcert_destroy (&client_cert);
  
    // retrieve server cert public key 
    zcert_t *server_cert = zcert_load (server_cert_path);
    if (!server_cert) {
        zsys_error ("could not load cert from '%s'", server_cert_path);
        return 1;
    }
    const char *server_key = zcert_public_txt (server_cert);
    zsock_set_curve_serverkey (client, server_key);
    zcert_destroy (&server_cert);

    // TODO: topics should be passed as argument
    zsock_set_subscribe (client, topic);

    //  service discovery lookup
    zactor_t *zdiscgo = zactor_new (zdiscgo_actor, NULL);
    zdiscgo_verbose (zdiscgo);
    rc = zdiscgo_load_plugin (zdiscgo, lib);
    if (rc != 0)
        return 1;

    endpoints = zdiscgo_discover (zdiscgo, url, key);
    if (verbose)
        zsys_debug ("attaching socket to %s", endpoints);
 
    // connect the socket
    rc = zsock_attach (client, endpoints, false);
    if (rc == -1) {
        zsys_error ("zsock_attach failed");
        zsock_destroy (&client);
        return 1;
    }

    // create poller
    if (verbose)
        zsys_debug ("creating poller");

    zpoller_t *poller = zpoller_new (client, NULL);
    if (!poller) {
        zsys_error ("failed to create poller");
        return 1;
    }

    // poll loop
    if (verbose)
        zsys_debug ("starting poll loop...");

    zsock_t *which = (zsock_t *) zpoller_wait (poller, -1);
    while (which == client) {
        zframe_t *frame = zframe_recv (which);
        if (frame) {
            zframe_t *normalized_frame = znormalizer_normalize (normalizer, frame);
            char *log = zframe_strdup (normalized_frame);
            zframe_destroy (&normalized_frame);
            printf ("%s\n", log);
            free (log);
        }
        which = (zsock_t *) zpoller_wait (poller, -1);
    }

    if (verbose)
        zsys_debug ("exiting poll loop...");
   
    // exit
    bool abnormal_exit = zpoller_terminated (poller);
   
    zpoller_destroy (&poller);
    zsock_destroy (&client);
    zactor_destroy (&zdiscgo);
    
    if (abnormal_exit) {
        zsys_error ("poller exited abnormally");
        return 1;
    }

    if (verbose)
        zsys_debug ("exiting program");

    return 0;
}
