/*  =========================================================================
    znormworker - Normalization worker

    Copyright (c) the Contributors as noted in the AUTHORS file.       
    This file is part of CZMQ, the high-level C binding for 0MQ:       
    http://czmq.zeromq.org.                                            
                                                                       
    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.           
    =========================================================================
*/

#ifndef ZNORMWORKER_H_INCLUDED
#define ZNORMWORKER_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif


//  @interface
//  Create new znormworker actor instance.
//  @TODO: Describe the purpose of this actor!
//
//      zactor_t *znormworker = zactor_new (znormworker, NULL);
//
//  Destroy znormworker instance.
//
//      zactor_destroy (&znormworker);
//
//  Enable verbose logging of commands and activity:
//
//      zstr_send (znormworker, "VERBOSE");
//
//  Start znormworker actor.
//
//      zstr_sendx (znormworker, "START", NULL);
//
//  Stop znormworker actor.
//
//      zstr_sendx (znormworker, "STOP", NULL);
//
//  This is the znormworker constructor as a zactor_fn;
NORMZ_EXPORT void
    znormworker_actor (zsock_t *pipe, void *args);

//  Self test of this actor
NORMZ_EXPORT void
    znormworker_test (bool verbose);
//  @end

#ifdef __cplusplus
}
#endif

#endif
